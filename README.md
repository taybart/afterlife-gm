# Afterlife GM


## Shortcuts

Right click to bring up `add` menu.


`{* -> click while key is pressed}` 


`e*` - switch between character and environment layers

`u*` - move element to top of layer

`d*` - move element to bottom of layer

`ESC*` - delete asset

`r` - rotate object clockwise π/2 rad // not working

`f*` - flip object along the y axis

`g` - toggle grid 
