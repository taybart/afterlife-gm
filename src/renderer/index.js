/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-unused-vars */
/* eslint no-param-reassign: ["error", { "props": false }] */
import * as P5 from 'p5';
import url from 'url';
import { ipcRenderer } from 'electron';
import path from 'path';
import process from 'process';
import MapObject from './map_object';

const isDev = process.env.NODE_ENV === 'development';

let id = 0;

// let globalX = 0;
// let globalY = 0;

/* eslint-disable no-undef */
const staticPath = val => (isDev ? url.resolve(window.location.origin, val)
  : path.join(__static, val));
/* eslint-enable no-undef */

const w = {
  x: 0,
  y: 0,
  width: 0,
  height: 0,
};

const s = (sk) => {
  let environment = [];
  let players = [];
  let enemies = [];
  const l = console;
  let grid;
  let showGrid = true;
  let envSelected = false;

  sk.setup = () => {
    sk.createCanvas(sk.windowWidth, sk.windowHeight);

    grid = sk.loadImage(staticPath('/assets/Iso_Grid.png'));
  };

  sk.draw = () => {
    sk.background(255);
    if (envSelected) {
      players.forEach(o => o.show(sk));
      enemies.forEach(o => o.show(sk));
    } else {
      environment.forEach(o => o.show(sk));
    }
    if (showGrid) {
      const rows = sk.width / grid.width;
      const cols = sk.height / grid.height;
      if (rows < 100 && cols < 100) {
        for (let i = 0; i <= rows; i += 1) {
          for (let j = 0; j <= cols; j += 1) {
            sk.image(grid, grid.width * i, grid.height * j);
          }
        }
      }
    }
    if (envSelected) {
      environment.forEach(o => o.show(sk));
    } else {
      players.forEach(o => o.show(sk));
      enemies.forEach(o => o.show(sk));
    }
  };


  sk.keyPressed = () => {
    if (sk.key === 'e') {
      envSelected = !envSelected;
    }
    if (sk.key === 'g') {
      showGrid = !showGrid;
    }
  };

  sk.mousePressed = () => {
    let arr = players.concat(enemies);

    if (envSelected) {
      arr = environment;
    }
    const found = arr.slice(0).reverse()
      .find(o => o.find(sk.mouseX, sk.mouseY));
    if (found) {
      if (sk.keyIsDown(70)) { // key: f
        found.flip = !found.flip;
      } else if (sk.keyIsDown(82)) { // key: r
        found.rotate += 1;
        found.rotate %= 4;
      } else if (sk.keyIsDown(68)) { // key: d
        if (!envSelected) {
          let i = players.findIndex(x => x.id === found.id);
          if (i < 0) {
            i = enemies.findIndex(x => x.id === found.id);
            enemies.unshift(enemies.splice(i, 1)[0]);
          } else {
            players.unshift(players.splice(i, 1)[0]);
          }
        } else {
          const i = environment.findIndex(x => x.id === found.id);
          environment.unshift(environment.splice(i, 1)[0]);
        }
      } else if (sk.keyIsDown(85)) { // key: u
        if (!envSelected) {
          let i = players.findIndex(x => x.id === found.id);
          if (i < 0) {
            i = enemies.findIndex(x => x.id === found.id);
            enemies.push(enemies.splice(i, 1)[0]);
          } else {
            players.push(players.splice(i, 1)[0]);
          }
        } else {
          const i = environment.findIndex(x => x.id === found.id);
          environment.push(environment.splice(i, 1)[0]);
        }
      } else if (sk.keyIsDown(sk.ESCAPE)) {
        if (!envSelected) {
          players = players.filter(x => x.id !== found.id);
          enemies = enemies.filter(x => x.id !== found.id);
        } else {
          environment = environment.filter(x => x.id !== found.id);
        }
      } else {
        found.drag = true;
        found.oX = found.x - sk.mouseX;
        found.oY = found.y - sk.mouseY;
      }
    }
  };

  sk.mouseReleased = () => {
    players.forEach((o) => { o.drag = false; });
    enemies.forEach((o) => { o.drag = false; });
    environment.forEach((o) => { o.drag = false; });
  };

  sk.windowResized = () => {
    sk.resizeCanvas(sk.windowWidth, sk.windowHeight);
  };

  ipcRenderer.on('loadPlayer', (event, arg) => {
    sk.loadImage(staticPath(`/assets/enemy/${arg}`), (img) => {
      players.push(new MapObject(id, img));
      id += 1;
    });
  });

  ipcRenderer.on('loadEnemy', (event, arg) => {
    sk.loadImage(staticPath(`/assets/enemy/${arg}`), (img) => {
      enemies.push(new MapObject(id, img));
      id += 1;
    });
  });

  ipcRenderer.on('loadEnv', (event, arg) => {
    sk.loadImage(staticPath(`/assets/map/${arg}`), (img) => {
      environment.push(new MapObject(id, img));
      id += 1;
    });
  });
};
const p5 = new P5(s);
