export default class MapObject {
  constructor(id, i, draggable) {
    this.id = id;
    this.draggable = draggable || true;
    this.i = i;
    // this.x = i.width / 2;
    // this.y = i.height / 2;
    this.x = 0;
    this.y = 0;
    this.width = i.width;
    this.height = i.height;
    this.oX = 0;
    this.oY = 0;

    // state
    this.rotate = 0;
    this.selected = false;
    this.drag = false;
    this.flip = false;
  }

  show(sk) {
    // if (this.x >= w.x && this.y >= w.y) {
    if (this.drag) {
      this.x = sk.mouseX + this.oX;
      this.y = sk.mouseY + this.oY;
    }

    sk.push();

    // sk.translate(this.x, this.y);
    // sk.rotate(this.rotate * (sk.PI / 2));

    if (this.flip) {
      sk.scale(-1, 1);
      // const s = sk.map(sk.mouseX, 0, this.width, -3, 3);
      // sk.scale(s, 3);
    }
    if (this.flip) {
      // sk.image(this.i, -(this.x + this.width / 2), this.y, this.width, this.height);
      sk.image(this.i, -(this.x + this.width), this.y, this.width, this.height);
    } else {
      // sk.image(this.i, -this.width / 2, -this.height / 2, this.width, this.height);
      sk.image(this.i, this.x, this.y, this.width, this.height);
    }

    if (this.flip) {
      sk.scale(-1, 1);
      // const s = sk.map(sk.mouseX, 0, this.width, -3, 3);
      // sk.scale(s, 3);
    }
    sk.pop();
    // }
  }

  find(mX, mY) {
    return (mX > this.x) && (mX < this.x + this.width)
      && (mY > this.y) && (mY < this.y + this.height)
      && this.draggable && !this.transparent(mX, mY);
    /* return (mX > (this.x - this.width / 2)) && (mX < this.x + (this.width / 2))
      && (mY > this.y - this.height / 2) && (mY < this.y + (this.height / 2))
        && this.draggable && !this.transparent(mX, mY); */
  }

  transparent(mX, mY) {
    this.i.loadPixels();
    // const x = mX - (this.x - this.width / 2);
    // const y = mY - (this.y - this.height / 2);
    const x = mX - (this.x);
    const y = mY - (this.y);
    let i = (4 * (y * this.width + x)) + 3;
    if (this.flip) {
      i = (4 * (y * this.width - x)) + 3;
    }
    return (this.i.pixels[i]) === 0;
  }
}
