/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-unused-vars */
/* eslint no-param-reassign: ["error", { "props": false }] */

import {
  app,
  Menu,
  shell,
  BrowserWindow,
} from 'electron';
import contextMenu from 'electron-context-menu';
import * as path from 'path';
import fs from 'fs';
import { format as formatUrl } from 'url';

// global reference to mainWindow (necessary to prevent window from being garbage collected)
let mainWindow;

const getFiles = (dir) => {
  const files = fs.readdirSync(dir);
  const fl = [];
  files.forEach((file) => {
    if (file !== '.DS_Store') {
      fl.push(file);
    }
  });
  return fl;
};

const formatFile = f => f.replace('_', ' ').split('.')[0];

/* eslint-disable no-undef */
const enemyPath = path.join(__static, '/assets/enemy');
const environmentPath = path.join(__static, '/assets/map/environment');
const propPath = path.join(__static, '/assets/map/prop');
const playerPath = path.join(__static, '/assets/player');
/* eslint-enable no-undef */

const enemy = getFiles(enemyPath);
const environment = getFiles(environmentPath);
const prop = getFiles(propPath);
const player = getFiles(playerPath);

const addAssetMenu = {
  label: 'Add',
  submenu: [
    {
      label: 'Enemy',
      submenu: enemy.map(f => ({
        label: formatFile(f),
        click() {
          mainWindow.webContents.send('loadEnemy', f);
        },
      })),
    },
    {
      label: 'Map',
      submenu: [
        {
          label: 'Environment',
          submenu: environment.map(f => ({
            label: formatFile(f),
            click() {
              mainWindow.webContents.send('loadEnv', `/environment/${f}`);
            },
          })),
        },
        {
          label: 'Prop',
          submenu: prop.map(f => ({
            label: formatFile(f),
            click() {
              mainWindow.webContents.send('loadEnv', `/prop/${f}`);
            },
          })),
        },
      ],
    },
    {
      label: 'Player',
      submenu: player.map(f => ({
        label: formatFile(f),
        click() {
          mainWindow.webContents.send('loadPlayer', `${playerPath}/${f}`);
        },
      })),
    },
  ],
};

const template = [
  {
    label: 'File',
    submenu: [addAssetMenu],
  }, {
    label: 'Edit',
    submenu: [
      { role: 'undo' },
      { role: 'redo' },
      { type: 'separator' },
      { role: 'cut' },
      { role: 'copy' },
      { role: 'paste' },
      { role: 'pasteandmatchstyle' },
      { role: 'delete' },
      { role: 'selectall' },
    ],
  }, {
    label: 'View',
    submenu: [
      { role: 'reload' },
      { role: 'forcereload' },
      { role: 'toggledevtools' },
      { type: 'separator' },
      { role: 'resetzoom' },
      { role: 'zoomin' },
      { role: 'zoomout' },
      { type: 'separator' },
      { role: 'togglefullscreen' },
    ],
  }, {
    role: 'window',
    submenu: [
      { role: 'minimize' },
      { role: 'close' },
    ],
  }, {
    role: 'help',
    submenu: [
      {
        label: 'Learn More',
        click() { shell.openExternal('https://electronjs.org'); },
      },
    ],
  },
];

if (process.platform === 'darwin') {
  template.unshift({
    label: app.getName(),
    submenu: [
      { role: 'about' },
      { type: 'separator' },
      { role: 'services' },
      { type: 'separator' },
      { role: 'hide' },
      { role: 'hideothers' },
      { role: 'unhide' },
      { type: 'separator' },
      { role: 'quit' },
    ],
  });

  // Edit menu
  template[1].submenu.push(
    { type: 'separator' },
    {
      label: 'Speech',
      submenu: [
        { role: 'startspeaking' },
        { role: 'stopspeaking' },
      ],
    },
  );

  // Window menu
  template[3].submenu = [
    { role: 'close' },
    { role: 'minimize' },
    { role: 'zoom' },
    { role: 'reload' },
    { role: 'forcereload' },
    { role: 'toggledevtools' },
    { type: 'separator' },
    { role: 'resetzoom' },
    { role: 'zoomin' },
    { role: 'zoomout' },
    { type: 'separator' },
    { role: 'togglefullscreen' },
    { type: 'separator' },
    { role: 'front' },
  ];
}

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);
contextMenu({ prepend: (params, browserWindow) => [addAssetMenu] });

const isDevelopment = process.env.NODE_ENV !== 'production';


function createMainWindow() {
  const window = new BrowserWindow();

  if (isDevelopment) {
    window.webContents.openDevTools();
    window.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`);
  } else {
    window.loadURL(formatUrl({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file',
      slashes: true,
    }));
  }

  window.on('closed', () => {
    mainWindow = null;
  });

  window.webContents.on('devtools-opened', () => {
    window.focus();
    setImmediate(() => {
      window.focus();
    });
  });

  return window;
}

// quit application when all windows are closed
app.on('window-all-closed', () => {
  // on macOS it is common for applications to stay open until the user explicitly quits
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // on macOS it is common to re-create a window even after all windows have been closed
  if (mainWindow === null) {
    mainWindow = createMainWindow();
  }
});

// create main BrowserWindow when electron is ready
app.on('ready', () => {
  mainWindow = createMainWindow();
});
